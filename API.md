# Stygos Damage Detection APIv1

- Contact us to create a user account. We will create an login id for you to use!

## Open Endpoints

- [Login](docs/login.md) :  `POST /api/login`

## Endpoints with Auth

- [Detect](docs/detect.md): `POST /api/v1/detect`
- [Results](docs/results.md): `POST /api/v1/results`
- [Result/:id](docs/result.md): `POST /api/v1/result/:id`

---------
####  Please contact us at info@stygos.com if you have any questions
