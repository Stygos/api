# Result/:id

Retrieve the results of a previously submitted image

**URL** : `/api/v1/result/:id`

**Method** : `GET`

**Auth required** : YES

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "id": "<uuid>",
    "damageCount": "<total detected damages count>",
    "maskedImage": "<base 64 encoded image with masks embedded on the image>",
    "boundingBoxes": "[list of bounding boxes of detected damages in format - [x,y,w,h]]",
    "confidenceScores": "[list of numbers indicating damage detection confidence between {0-1}]",
    "damageLength": "[If scale is provided, estimated damage length in cm is returned]"
}
```

## Error Response

**Condition** : If id is not found.

**Code** : `404 BAD REQUEST`

**Content** :

```json
{
    "non_field_errors": [
        "Please submit a new id."
    ]
}
```

## Error Response

**Condition** : If 'auth token' is wrong.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
    "non_field_errors": [
        "Unable to identify user, please login again."
    ]
}
```
