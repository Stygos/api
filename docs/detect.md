# Detect

Used to detect the damages on the submitted image.

**URL** : `/api/v1/detect`

**Method** : `POST`

**Auth required** : YES

**Data constraints**

```json
{
    "image": "<base64 encoded image>",
    "authToken" : "Token from auth",
    "detectConfidence": "[Optional parameter]",
    "scale": "[Optional parameter]"
}
```

**Data example**

```json
{
    "image": "<base64 encoded image>",
    "authToken" : "93144b288eb1fdccbe46d6fc0f241a51766ecd3d",
    "detectConfidence": "0.8",
    "scale": "5",
}
```

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "damageCount": "<total detected damages count>",
    "maskedImage": "<base 64 encoded image with masks embedded on the image>",
    "boundingBoxes": "[list of bounding boxes of detected damages in format - [x,y,w,h]]",
    "confidenceScores": "[list of numbers indicating damage detection confidence between {0-1}]",
    "damageLength": "[If scale is provided, estimated damage length in cm is returned]"
}
```

## Error Response

**Condition** : If 'image' is corrupt.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
    "non_field_errors": [
        "Please submit a new image."
    ]
}
```

## Error Response

**Condition** : If 'auth token' is wrong.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
    "non_field_errors": [
        "Unable to identify user, please login again."
    ]
}
```