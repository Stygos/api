# Results

Retrieve previously stored results

**URL** : `/api/v1/results`

**Method** : `GET`

**Auth required** : YES

## Success Response

**Code** : `200 OK`

**Content example**

```json
{
    "results": "[list of result ids",
}
```

## Error Response

**Condition** : If 'auth token' is wrong.

**Code** : `400 BAD REQUEST`

**Content** :

```json
{
    "non_field_errors": [
        "Unable to identify user, please login again."
    ]
}
`
